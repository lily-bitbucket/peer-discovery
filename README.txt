CS6378 Assignment 1
Name: Shuangli Wu
NetID: sxw133730

The algorithm used is gossip-based. Each peer contact their neighbors
to get their knowledge about the network. The time complexity of this
algorithm is O(d), where d is the diameter of the initial network
graph. 

# Compile
javac peerdisc/*.java

# Deploy
cd peerdisc_res
./deploy.sh config_lab.txt sxw133730 SCTPProvider
OR
./deploy.sh config_lab.txt sxw133730 TCPProvider

# Run
cd peerdisc_res
./run.sh config_lab.txt sxw133730 SCTPProvider
OR
./deploy.sh config_lab.txt sxw133730 TCPProvider