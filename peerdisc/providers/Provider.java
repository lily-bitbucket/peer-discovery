package peerdisc.providers;

import java.util.Set;

public interface Provider {
	public void spreadIntel() throws Exception;

	public Set<String> getIntel(String peer) throws Exception;
}
