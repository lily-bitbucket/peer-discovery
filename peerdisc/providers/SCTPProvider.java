package peerdisc.providers;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.sun.nio.sctp.*;

import peerdisc.Constants;
import peerdisc.PeerDisc;

public class SCTPProvider implements Provider {
	PeerDisc peerDisc;

	public SCTPProvider(PeerDisc peerDisc) {
		this.peerDisc = peerDisc;
	}

	public void spreadIntel() throws Exception {
		SctpServerChannel sctpServerChannel = SctpServerChannel.open();
		InetSocketAddress serverAddr = new InetSocketAddress(peerDisc.portNum);
		sctpServerChannel.bind(serverAddr);
		while (true) {
			ByteBuffer byteBuffer = ByteBuffer
					.allocate(Constants.SCTP_MESSAGE_SIZE);
			SctpChannel sctpChannel = accept(sctpServerChannel);
			MessageInfo messageInfo = MessageInfo.createOutgoing(null, 0);

			String message = "";
			message += peerDisc.knownPeers.size() + " ";
			for (String knownPeer : peerDisc.knownPeers)
				message += knownPeer + " ";
			byteBuffer.put(message.getBytes());
			peerDisc.logger.info("Message to send: " + message);
			byteBuffer.flip();
			sctpChannel.send(byteBuffer, messageInfo);
		}
	}

	private SctpChannel accept(final SctpServerChannel sctpServerChannel)
			throws Exception {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<SctpChannel> sctpChannel = executor.invokeAll(
				Arrays.asList(new Callable<SctpChannel>() {

					@Override
					public SctpChannel call() throws Exception {
						return sctpServerChannel.accept();
					}

				}), 10, TimeUnit.SECONDS).get(0);
		executor.shutdown();
		return sctpChannel.get();
	}

	public Set<String> getIntel(String peer) throws Exception {
		ByteBuffer byteBuffer = ByteBuffer
				.allocate(Constants.SCTP_MESSAGE_SIZE);
		String[] nameAndPort = peer.split(":");
		SocketAddress socketAddress = new InetSocketAddress(nameAndPort[0],
				Integer.valueOf(nameAndPort[1]));
		SctpChannel sctpChannel = SctpChannel.open();
		InetSocketAddress clientBindAddress = new InetSocketAddress(
				Integer.valueOf(nameAndPort[1]) + 1000);
		sctpChannel.bind(clientBindAddress);
		sctpChannel.connect(socketAddress);
		sctpChannel.receive(byteBuffer, null, null);
		sctpChannel.close();
		String message = byteToString(byteBuffer);
		String tokens[] = message.split(" ");
		Set<String> result = new HashSet<String>();
		int peerNum = Integer.parseInt(tokens[0]);
		for (int i = 1; i <= peerNum; i++)
			result.add(tokens[i]);
		peerDisc.logger.info("Received: " + result);
		return result;
	}

	private String byteToString(ByteBuffer byteBuffer) {
		byteBuffer.position(0);
		byteBuffer.limit(Constants.SCTP_MESSAGE_SIZE);
		byte[] bufArr = new byte[byteBuffer.remaining()];
		byteBuffer.get(bufArr);
		return new String(bufArr);
	}
}
