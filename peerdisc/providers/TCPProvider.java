package peerdisc.providers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

import peerdisc.PeerDisc;

public class TCPProvider implements Provider {
	PeerDisc peerDisc;

	public TCPProvider(PeerDisc peerDisc) {
		this.peerDisc = peerDisc;
	}

	public void spreadIntel() throws Exception {
		ServerSocket serverSock = new ServerSocket(peerDisc.portNum);
		serverSock.setSoTimeout(10000);
		while (!peerDisc.done) {
			Socket sock = serverSock.accept();
			PrintWriter writer = new PrintWriter(sock.getOutputStream());
			for (String knownPeer : peerDisc.knownPeers)
				writer.println(knownPeer);
			writer.close();
		}
		serverSock.close();
	}

	public Set<String> getIntel(String peer) throws Exception {
		String[] nameAndPort = peer.split(":");
		Socket clientSocket = new Socket(nameAndPort[0],
				Integer.valueOf(nameAndPort[1]));
		clientSocket.setSoTimeout(2000);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				clientSocket.getInputStream()));
		Set<String> result = new HashSet<String>();
		String line = null;
		while ((line = reader.readLine()) != null) {
			result.add(line);
		}
		reader.close();
		clientSocket.close();
		return result;
	}
}
