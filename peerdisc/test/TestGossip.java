package peerdisc.test;

import java.util.*;
import org.junit.Test;
import peerdisc.*;

public class TestGossip {

	Random rand = new Random();

	@Test
	public void test_gossip_tcp() throws InterruptedException {
		List<Thread> threads = new ArrayList<Thread>();
		for (int i = 0; i < 10; i++) {
			final int ii = i;
			Thread t = new Thread() {
				@Override
				public void run() {
					try {
						PeerDisc.main(new String[] { "" + ii });
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
			t.start();
			// Introduce some delay between node appearing
			Thread.sleep((Math.abs(rand.nextLong()) + 1000) % 5000);
			threads.add(t);
		}
		for (Thread t : threads)
			t.join();
	}

}
