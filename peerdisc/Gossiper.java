package peerdisc;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import peerdisc.Constants;
import peerdisc.PeerDisc;
import peerdisc.providers.Provider;

class Gossiper implements Runnable {
	Logger logger;
	Set<String> knownPeers;
	PeerDisc peerDisc;
	Set<String> bootstrapPeers = new HashSet<String>();
	Provider provider;

	public Gossiper(PeerDisc peerDisc, Provider provider) {
		this.peerDisc = peerDisc;
		this.logger = peerDisc.logger;
		this.provider = provider;
		knownPeers = peerDisc.knownPeers;
		bootstrapPeers.addAll(knownPeers);
		logger.info("Bootstrap peers: " + bootstrapPeers);
	}

	@Override
	public void run() {
		logger.info("***** Gossiper thread started");
		try {
			int n = 0;
			int noUpdateCount = 0;
			boolean bootstrapPeersAllReady = false;
			while (n < Constants.MAXIMUM_RETRIES
					&& noUpdateCount < Constants.UNTIL_CONVERGED) {
				logger.info("Gossiping round: " + n + ", noUpdateCount: "
						+ noUpdateCount);
				boolean updated = false;
				// If non-bootstrap peers departed, it means they are not
				// "not-ready", but finished their discovery
				boolean bootstrapPeersFailed = false;
				for (String knownPeer : knownPeers) {
					try {
						logger.info("Gossiping with neighbor: " + knownPeer);

						Set<String> intel = provider.getIntel(knownPeer);
						logger.info("Intel from neighbor: " + intel);
						Set<String> update = calcUpdate(intel, knownPeers);
						logger.info("Updates from neighbor: " + update);
						if (!update.isEmpty()) {
							knownPeers.addAll(update);
							updated = true;
							noUpdateCount = 0;
						}
					} catch (Exception e) {
						logger.log(Level.SEVERE, e.getMessage(), e);
						logger.info("Cannot connect to peer: " + knownPeer);
						if (!bootstrapPeers.contains(knownPeer)) {
							logger.info("Peer departure (non-bootstrap) happened: "
									+ knownPeer);
							break;
						} else
							bootstrapPeersFailed = true;
					}
				}

				if (bootstrapPeersFailed == false)
					bootstrapPeersAllReady = true;

				logger.info("Updates in this round? " + updated);
				if (!updated && bootstrapPeersAllReady)
					noUpdateCount++;

				// Non bootstrap peer departure means they have finished
				// if (nonBootstrapPeerDeparture)
				// break;

				logger.info("Known peers: " + knownPeers);
				Thread.sleep(Constants.RETRY_INTERVAL);
				n++;
			}

			peerDisc.done = true;
			logger.info("***** Final result: " + knownPeers);

		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
		}
		logger.info("***** Gossiper thread done");
	}

	private Set<String> calcUpdate(Set<String> intel, Set<String> known) {
		Set<String> update = new HashSet<String>();
		for (String s : intel)
			if (!known.contains(s))
				update.add(s);
		return update;
	}
}
