package peerdisc;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import peerdisc.Constants;
import peerdisc.Dispatcher;
import peerdisc.providers.Provider;

public class PeerDisc {
	Runnable listener;
	Runnable gossiper;
	String selfIP;
	public int portNum;
	public volatile Set<String> knownPeers = new ConcurrentSkipListSet<String>();
	public volatile boolean done = false;
	public Logger logger;

	public void initLogger() {
		logger = Logger.getLogger("logger-" + portNum);
		FileHandler fh;
		try {
			fh = new FileHandler("peer_" + portNum + ".log");
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public PeerDisc(List<String> bootstrapPeers, String selfIP, int portNum,
			String providerClsName) throws Exception {
		this.portNum = portNum;
		this.selfIP = selfIP;

		initLogger();

		knownPeers.add(selfIP + ":" + portNum);
		knownPeers.addAll(bootstrapPeers);

		// Initialize listener and gossiper
		Class<?> clsProvider = Class.forName("peerdisc.providers." + providerClsName);
		Constructor<?> consProvider = clsProvider
				.getConstructor(PeerDisc.class);
		Provider provider = (Provider) consProvider.newInstance(this);
		gossiper = new Gossiper(this, provider);
		listener = new Listener(this, provider);

		logger.info("***** I am " + selfIP + ":" + portNum);
	}

	public void go() {
		try {
			Thread lt = new Thread(listener);
			lt.start();
			Thread gt = new Thread(gossiper);
			gt.start();
			logger.info("***** Now waiting for the algorithm to finish");
			lt.join();
			gt.join();
			logger.info("***** All done");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		int id = Integer.parseInt(args[0]);
		String providerClsName = Constants.DEF_PROVIDER_CLS_NAME;
		String bootstrapFileName = Constants.DEF_BOOTSTRAP_FILENAME;
		if (args.length == 2) {
			bootstrapFileName = args[1];
		}
		if (args.length == 3) {
			bootstrapFileName = args[1];
			providerClsName = args[2];
		}
		Dispatcher dispatcher = new Dispatcher(bootstrapFileName, id);
		PeerDisc peerDisc = new PeerDisc(dispatcher.getBootstrapPeers(),
				dispatcher.getSelfIP(), dispatcher.getPortNum(),
				providerClsName);
		peerDisc.go();
	}
}