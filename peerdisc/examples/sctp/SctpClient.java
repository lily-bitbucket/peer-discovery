package peerdisc.examples.sctp;

import java.io.*;
import java.net.*;

import com.sun.nio.sctp.*;

import java.nio.*;

public class SctpClient {
	public static final int MESSAGE_SIZE = 100;

	public void go() {
		ByteBuffer byteBuffer = ByteBuffer.allocate(MESSAGE_SIZE);
		try {
			SocketAddress socketAddress = new InetSocketAddress("localhost",
					5000);
			SctpChannel sctpChannel = SctpChannel.open();
			sctpChannel.bind(new InetSocketAddress(5001));
			sctpChannel.connect(socketAddress);
			sctpChannel.receive(byteBuffer, null, null);
			String message = byteToString(byteBuffer);
			System.out.println(message);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public String byteToString(ByteBuffer byteBuffer) {
		byteBuffer.position(0);
		byteBuffer.limit(MESSAGE_SIZE);
		byte[] bufArr = new byte[byteBuffer.remaining()];
		byteBuffer.get(bufArr);
		return new String(bufArr);
	}

	public static void main(String args[]) {
		SctpClient SctpClientObj = new SctpClient();
		SctpClientObj.go();
	}
}
