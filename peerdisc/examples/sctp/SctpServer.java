package peerdisc.examples.sctp;

import java.io.*;
import java.net.*;

import com.sun.nio.sctp.*;

import java.nio.*;

public class SctpServer {
	public static final int MESSAGE_SIZE = 100;

	public void go() {
		try {
			SctpServerChannel sctpServerChannel = SctpServerChannel.open();
			InetSocketAddress serverAddr = new InetSocketAddress(5000);
			sctpServerChannel.bind(serverAddr);
			while (true) {
				ByteBuffer byteBuffer = ByteBuffer.allocate(MESSAGE_SIZE);
				SctpChannel sctpChannel = sctpServerChannel.accept();
				MessageInfo messageInfo = MessageInfo.createOutgoing(null, 0);
				byteBuffer.put("Hello, SCTP!".getBytes());
				byteBuffer.flip();
				sctpChannel.send(byteBuffer, messageInfo);
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void main(String args[]) {

		SctpServer SctpServerObj = new SctpServer();
		SctpServerObj.go();
	}

}
