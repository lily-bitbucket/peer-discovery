package peerdisc;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Dispatcher {

	List<String> bootstrapPeers = new ArrayList<String>();
	int portNum;
	String selfIP;

	public Dispatcher(String bootstrapFileName, int id)
			throws FileNotFoundException {
		Scanner scan = new Scanner(new File(bootstrapFileName));
		Map<Integer, String> peerMap = new HashMap<Integer, String>();
		while (scan.hasNextLine()) {
			String line = scan.nextLine();
			if (line.startsWith("#"))
				continue;
			String[] tokens = line.split("\\s");
			peerMap.put(Integer.parseInt(tokens[0]), tokens[1] + ":" + tokens[2]);
		}
		scan.close();

		scan = new Scanner(new File(bootstrapFileName));
		while (scan.hasNextLine()) {
			String line = scan.nextLine();
			if (line.startsWith("#"))
				continue;
			String[] tokens = line.split("\\s");
			int lineId = Integer.parseInt(tokens[0]);
			// Found configuration for the given id
			if (lineId == id) {
				for (int i = 3; i < tokens.length; i++) {
					int index = Integer.parseInt(tokens[i]);
					bootstrapPeers.add(peerMap.get(index));
				}
				portNum = Integer.parseInt(tokens[2]);
				selfIP = tokens[1];
			}
		}
		scan.close();
	}

	public List<String> getBootstrapPeers() {
		return bootstrapPeers;
	}

	public int getPortNum() {
		return portNum;
	}

	public String getSelfIP() {
		return selfIP;
	}

}
