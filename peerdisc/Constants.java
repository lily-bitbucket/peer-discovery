package peerdisc;

public class Constants {
	public static final int MAXIMUM_RETRIES = 100;
	public static final int RETRY_INTERVAL = 2000;
	public static final int UNTIL_CONVERGED = 5;
	public static final String DEF_PROVIDER_CLS_NAME = "TCPProvider";
	public static final String DEF_BOOTSTRAP_FILENAME = "peerdisc_res/config_junit.txt";
	public static final int SCTP_MESSAGE_SIZE = 1000;
}
