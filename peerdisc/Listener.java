package peerdisc;

import java.util.logging.Level;

import peerdisc.providers.Provider;

class Listener implements Runnable {
	PeerDisc peerDisc;
	Provider provider;

	public Listener(PeerDisc peerDisc, Provider provider) {
		this.peerDisc = peerDisc;
		this.provider = provider;
	}

	@Override
	public void run() {
		peerDisc.logger.info("***** Listener thread started");
		try {
			peerDisc.logger.info("Known peers: " + peerDisc.knownPeers);
			provider.spreadIntel();
		} catch (Exception e) {
			peerDisc.logger.log(Level.SEVERE, e.getMessage(), e);
		}
		peerDisc.logger.info("***** Listener thread done");
	}
}
