#!/bin/bash

configFile=$1
netid=sxw133730
if [[ -n $2 ]]; then
   netid=$2 
fi

while read line
do
    if [[ $line == "#"* ]]; then
      continue
    fi
    host=$( echo $line | awk '{ print $2 }' )
    port=$( echo $line | awk '{ print $3 }' )
    echo "Deploying to $host:$port"
    ssh $netid@$host "/bin/mkdir ~/cs6378;/bin/rm -rf ~/cs6378/peerdisc*" < /dev/null > /dev/null 2>&1
    scp -r ../peerdisc* $netid@$host:~/cs6378/ > /dev/null 2>&1
    echo "Only need to deploy once to my home folder"
    break
done < $configFile
