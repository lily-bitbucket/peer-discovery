#!/bin/bash

configFile=$1
netid=sxw133730
if [[ -n $2 ]]; then
    netid=$2
fi

while read line
do
    if [[ $line == "#"* ]]; then
      continue
    fi
    host=$( echo $line | awk '{ print $2 }' )
    port=$( echo $line | awk '{ print $3 }' )
    echo "Copying log file for $host:$port"
    scp $netid@$host:~/cs6378/peer_$port.log ~ > /dev/null 2>&1
done < $configFile
