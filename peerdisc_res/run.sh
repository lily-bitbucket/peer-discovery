#!/bin/bash

configFile=$1
netid=sxw133730
if [[ -n $2 ]]; then
    netid=$2
fi

while read line
do
    if [[ $line == "#"* ]]; then
      continue
    fi
    id=$( echo $line | awk '{ print $1 }' )
    host=$( echo $line | awk '{ print $2 }' )
    port=$( echo $line | awk '{ print $3 }' )
    echo "Running on $host:$port"
    ssh -f $netid@$host "cd ~/cs6378;java peerdisc/PeerDisc $id peerdisc_res/$configFile $3 &" < /dev/null > /dev/null 2>&1
done < $configFile
